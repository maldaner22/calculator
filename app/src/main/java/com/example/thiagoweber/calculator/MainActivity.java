package com.example.thiagoweber.calculator;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView calculation, result;
    Float x, y, z;
    boolean operationplus = false, operationminus = false, operationmultiplied = false, operationdivided = false;
    int pointcheck = 0;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.logoapp);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        calculation = (TextView) findViewById(R.id.calculation);
        result = (TextView) findViewById(R.id.result);
        mp = MediaPlayer.create(this, R.raw.sound);
        mp.start();
    }

    public void clickzero(View v) {
        result.setText(result.getText() + "0");
    }

    public void clickdoublezero(View v) {
        result.setText(result.getText() + "00");
    }

    public void clickone(View v) {
        result.setText(result.getText() + "1");
    }

    public void clicktwo(View v) {
        result.setText(result.getText() + "2");
    }

    public void clickthree(View v) {
        result.setText(result.getText() + "3");
    }

    public void clickfour(View v) {
        result.setText(result.getText() + "4");
    }

    public void clickfive(View v) {
        result.setText(result.getText() + "5");
    }

    public void clicksix(View v) {
        result.setText(result.getText() + "6");
    }

    public void clickseven(View v) {
        result.setText(result.getText() + "7");
    }

    public void clickeight(View v) {
        result.setText(result.getText() + "8");
    }

    public void clicknine(View v) {
        result.setText(result.getText() + "9");
    }

    public void clickpoint(View v) {
        if (pointcheck == 0) {
            result.setText(result.getText() + ".");
            pointcheck++;
        }
    }

    public void operation(View o, String temp) {
        String aux = result.getText().toString();
        calculation.setText(aux + temp);
        x = Float.valueOf(aux);
        result.setText(null);
        pointcheck = 0;
    }

    public void clickplus(View a) {
        String aux = result.getText().toString();
        if (!aux.isEmpty() && !("-".equals(aux)) && !(".".equals(aux))) {
            operation(a, "+");
            operationplus = true;
        }
    }

    public void clickminus(View b) {
        String aux = result.getText().toString();
        if (aux.isEmpty()) {
            result.setText("-");
        } else {
            if (("-".equals(aux))) {
                result.setText("-");
            } else {
                operation(b, "-");
                operationminus = true;
            }
        }
    }

    public void clickmultiplied(View c) {
        String aux = result.getText().toString();
        if (!aux.isEmpty() && !("-".equals(aux)) && !(".".equals(aux))) {
            operation(c, "x");
            operationmultiplied = true;
        }
    }

    public void clickdivided(View d) {
        String aux = result.getText().toString();
        if (!aux.isEmpty() && !("-".equals(aux)) && !(".".equals(aux))) {
            operation(d, "÷");
            operationdivided = true;
        }
    }

    public void clickequals(View e) {
        String aux = result.getText().toString();
        if ((operationplus == true || operationminus == true || operationdivided == true || operationmultiplied == true) && !aux.isEmpty() && !("-".equals(aux)) && !(".".equals(aux))) {
            String auxCal = calculation.getText().toString();
            String auxRes = result.getText().toString();
            calculation.setText(auxCal + auxRes);
            y = Float.valueOf(auxRes);
            if (y == 0 && operationdivided == true) {
                clearall();
                error();
                operationdivided = false;
            } else {
                operationequals(e);
                pointcheck = 0;
            }
        }
    }

    public void operationequals(View f) {
        if (operationplus == true) {
            z = x + y;
            showresult();
            operationplus = false;
        }
        if (operationminus == true) {
            z = x - y;
            showresult();
            operationminus = false;
        }
        if (operationmultiplied == true) {
            z = x * y;
            showresult();
            operationmultiplied = false;
        }
        if (operationdivided == true) {
            z = x / y;
            showresult();
            operationdivided = false;
        }
    }

    public void showresult() {
        String show = Float.toString(z);
        String temp = show.substring(Math.max(show.length() - 2, 0));
        if (".0".equals(temp)) {
            result.setText(show.replace(".0", ""));
        } else {
            result.setText(show);
        }
    }

    public void clickclean(View t) {
        clearall();
    }

    public void clickbackspace(View x) {
        String aux = result.getText().toString();
        if (aux != null && aux.length() > 0) {
            aux = aux.substring(0, aux.length() - 1);
            result.setText(aux);
        }
    }

    public void clickspeaker(View s) {
        ImageButton speaker = (ImageButton) findViewById(R.id.speaker);
        if (mp.isPlaying()) {
            mp.pause();
            speaker.setImageResource(R.drawable.sound);
        } else {
            mp.start();
            speaker.setImageResource(R.drawable.muted);
        }
    }

    public void clearall() {
        result.setText(null);
        calculation.setText(null);
        x = null;
        y = null;
        pointcheck = 0;
    }

    public void error() {
        Toast.makeText(MainActivity.this, "Impossível dividir por 0!", Toast.LENGTH_SHORT).show();
        Vibrator vibra = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibra.vibrate(500);
    }
}